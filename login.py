class Login:
    def __init__(self, dal):
        self.dal = dal

    def loggin_in(self):
        print("\n" * 20)
        naam = input("Welkom bij het inlogscherm.\nTyp uw naam om in te loggen?").lower()
        rights = self.dal.login(naam)
        return naam, rights
