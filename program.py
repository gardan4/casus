from login import Login
from dal import Dal
from products import Products


# Hoofdmenu
class Program:
    def __init__(self):
        self.dal = Dal()
        self.login = Login(self.dal)
        self.products = Products(self.dal)
        self.main()

    def main(self):
        self.naam, self.rights = self.login.loggin_in()
        if self.rights == 1:
            self.menu_admin()
        else:
            self.menu_user()

    def menu_user(self):
        while True:
            question = "default"
            while question != "0":
                print("\n" * 20)
                question = input("Welkom bij het hoofdmenu.\n"
                                 "1) Browse producten\n"
                                 "2) Bestellingen\n"
                                 "3) Profielinformatie bekijken/veranderen\n"
                                 "0) Uitloggen")
                if question == "1":
                    self.menu_2()
                elif question == "2":
                    self.menu_3()
                elif question == "3":
                    pass
                elif question == "0":
                    pass
                else:
                    input("Dit was geen geldige optie. Druk op enter om opnieuw te kiezen.")
            self.main()

    def menu_admin(self):
        while True:
            question = "default"
            while question != "0":
                print("\n" * 20)
                question = input("Welkom bij het hoofdmenu.\n"
                                 "1) Producten toevoegen aan assortiment\n"
                                 "2) \n"
                                 "3) Profielinformatie bekijken/veranderen\n"
                                 "0) Uitloggen")
                if question == "1":
                    self.menu_1()
                elif question == "2":
                    pass
                elif question == "3":
                    pass
                elif question == "4":
                    pass
                elif question == "0":
                    pass
                else:
                    input("Dit was geen geldige optie. Druk op enter om opnieuw te kiezen.")
            self.main()

    # Add listing
    def menu_1(self):
        print("\n" * 20)
        self.products.add_listing()

    # Browse listings
    def menu_2(self):
        print("\n" * 20)
        self.products.view_products(self.naam)

    def menu_3(self):
        users = self.dal.read_user()
        for i in range(0, len(users)):
            if self.naam == users[i][0]:
                for g in range(2, len(users[i])):
                    print(f"{g-1}) {users[i][g]}")


    def menu_4(self):
        pass

    def menu_5(self):
        pass

    def menu_6(self):
        pass


# Start van het programma.
if __name__ == '__main__':
    Program()
