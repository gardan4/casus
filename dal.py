import pandas as pd
import pickle
import time


class Dal:
    def __init__(self):
        self.userDatabase = 'userDatabase'
        self.listingDatabase = 'listingDatabase'

    def login(self, naam):
        users = self.read_user()
        registered = False
        for i in users:
            if naam == i[0]:
                if i[1] == 1:
                    print("U wordt ingelogd als fabrikant, een moment geduld.")
                    print("\n" * 20)
                    rights = i[1]
                    registered = True
                    time.sleep(2)
                elif i[1] == 0:
                    print("U wordt ingelogd als klant, een moment geduld.")
                    print("\n" * 20)
                    rights = i[1]
                    registered = True
                    time.sleep(2)
        if not registered:
            rights = int(input("U bent nog niet geregistreerd\n bent u een klant(0), of een producent(1)?: "))
            print("Uw account wordt aangemaakt 1 moment geduld.")
            users.append([naam, rights])
            self.write_user(users)
            time.sleep(2)
        return rights

    def read_user(self):
        try:
            with open(self.userDatabase, "rb") as file:
                users = pickle.load(file)
        except (EOFError, FileNotFoundError):
            user = [["user", 0]]
            self.write_user(user)
            with open(self.userDatabase, "rb") as file:
                users = pickle.load(file)
        return users

    def write_user(self, users):
        with open(self.userDatabase, "wb") as file:
            pickle.dump(users, file)

    def read_listing(self):
        try:
            with open(self.listingDatabase, "rb") as file:
                listings = pickle.load(file)
        except (EOFError, FileNotFoundError):
            listing = [["product", "min price", "mat", "img", "size"]]
            self.write_listing(listing)
            with open(self.listingDatabase, "rb") as file:
                listings = pickle.load(file)
        print(listings)
        return listings

    def write_listing(self, listing):
        with open(self.listingDatabase, "wb") as file:
            pickle.dump(listing, file)
