class Products():
    def __init__(self, dal):
        self.dal = dal

    def add_listing(self):
        product = input("Naam product: ")
        price = input("Min prijs: ")

        listing = [product, price, 0, 0, 0]
        listings = self.dal.read_listing()
        listings.append(listing)
        self.dal.write_listing(listings)

    def print_listings(self):
        listings = self.dal.read_listing()
        for i in range(1, len(listings)):
            print(f"{i}) product: {listings[i][0]}, Min prijs: {listings[i][1]}")

    def view_products(self, naam):
        self.print_listings()
        listings = self.dal.read_listing()
        product = int(input("kies welke"))
        print(f"Product: {listings[product][0]}\n")
        mat = input("1) Materiaal(Comp1/Comp2/Comp3): ")
        img = input("2) Afbeelding toevoegen(Y/N):")
        size = input(f"3)Maat(S/M/L)")
        listings[product][2] = mat
        listings[product][3] = img
        listings[product][4] = size
        inp = input("proceed?(YN)")
        if inp.lower() == "y":
            users = self.dal.read_user()
            for i in range(0, len(users)):
                if naam == users[i][0]:
                    users[i].append(listings[product])
                    self.dal.write_user(users)
                    print(users)

